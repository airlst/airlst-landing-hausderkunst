angular.module('app', [
        'oitozero.ngSweetAlert',
        'timer',
        'AirLST-SDK'
    ])
    .directive('ngEnter', [
        function() {
            return function(scope, element, attrs) {
                element.bind("keydown keypress", function(event) {
                    if (event.which === 13) {
                        scope.$apply(function() {
                            scope.vm.loadRsvp();
                        });

                        event.preventDefault();
                    }
                });
            };
        }
    ])

    .controller('ContactFormController', ['$scope', '$http', '$compile', '$timeout', 'SweetAlert',

        function($scope, $http, $compile, $timeout, SweetAlert) {

            $scope.contactform = {};
            $scope.contact_message = "";
            $scope.errormsg = "";
            $scope.message_sent = false;
            $scope.contact_topic = "";

            $scope.formValidationCf = false;

            $scope.contactFormSubmit = function() {

                // var complete = true;

                $scope.formValidationCf = true;

                if ($scope.contactForm.$valid) {
                    // complete = false;
                    $scope.formValidationCf = false;

                    $http({
                            url: 'https://wrapper.airlst.com/test',
                            method: "POST",
                            data: {
                                'titel': $scope.contactform.titel,
                                'anrede': $scope.contactform.sex,
                                'first_name': $scope.contactform.vorname,
                                'last_name': $scope.contactform.nachname,
                                'email': $scope.contactform.email,
                                'message': $scope.contactform.contact_message
                            }
                        })
                        .then(function(response) {
                                // success
                                $scope.message_sent = true;
                            },
                            function(response) { // optional
                                // failed
                                alert('Ihre Nachricht konnte nicht gesendet werden. Bitte wenden Sie sich per E-Mail an Ihren Ansprechpartner');
                            });
                } else {
                    $scope.compiledErrors = '<div><ul><li ng-repeat="(key, errors) in contactForm.$error track by $index">  <ul>' +
                        '<li ng-repeat="e in errors | unique:\'$name\'">{{ e.$name }} <span ng-show="key == \'required\'">ist ein Pflichtfeld.</span><span ng-show="key == \'email\'">ist falsch.</span></li>' +
                        '</ul> </li></ul></div>';
                    $scope.errors = $compile($scope.compiledErrors)($scope);

                    $timeout(function() {
                        SweetAlert.swal({
                            html: true,
                            title: '<h1>Missing data</h1>',
                            text: $scope.errors[0].outerHTML,
                            confirmButtonText: 'OK',
                            customClass: 'cancel-alert',
                            animation: false
                        });
                    });
                }

                // if($scope.contactform.contact_message == ""){
                //
                //     $scope.errormsg = "Bitte vor dem Senden eine Nachricht eingeben.";
                // }

                // if(complete){
                //
                // }
            };

        }
    ])

    .controller('AirLSTCtrl', ['$scope', '$http', '$location', 'SweetAlert', 'AirLSTSdkService', function($scope, $http, $location, SweetAlert, AirLSTSdkService) {
        var vm = this;

        vm.hasError = false;
        vm.loading = true;
        vm.submitting = false;
        vm.currentView = "code";
        vm.currentTab = "start";
        vm.countdownTime = 0;

        /**
         * Initializes the application and loads all necessary data from the server
         *
         * @private
         */
        vm._init = function() {
            AirLSTSdkService.companyUid = '35VC27Y';
            AirLSTSdkService.guestlistUid = 'PBAWUCRYWR';
            AirLSTSdkService.apiUrl = 'https://v1.api.airlst.com/lp';

            vm._resetRsvpInformation();
            vm._resetRsvpCode();
            vm._resetAnswerType();
            vm._initCountDown();
            vm._retrieveEventInformation();
        };

        // Public functions

        /**
         * Queries the airlst server for a rsvp
         *
         * vm.rsvpCode has to be set to a valid rsvp code for this call to succeed. If the API responds with an rsvp,
         * the 'content' view will be loaded next.
         *
         * If an error occurs during the API call, rsvp information is reset an error message will be displayed.
         *
         * @returns {PromiseLike<T | never> | Promise<T | never> | *}
         */
        vm.loadRsvp = function() {
            vm.loading = true;
            return AirLSTSdkService.retrieveRsvpInformation(vm.rsvpCode).then(function(rsvpInfo) {
                vm.loading = false;
                vm.rsvpInformation = vm._prepareRsvpInformationFromApi(rsvpInfo);

                switch (vm._checkRsvpActionOnUrl()) {
                    case 'cancel':
                        vm.cancelRsvp();
                        break;
                    default:
                        vm.goToView('content');
                        break;
                }

            }, function(error) {
                vm.loading = false;
                vm.hasError = true;
                vm._resetRsvpInformation();

                switch (error.identifier) {
                    case 'rsvp_not_found':
                        SweetAlert.swal('Code not found', 'Please enter a valid code.', 'error');
                        break;
                    default:
                        SweetAlert.swal('Error', 'Code kann nicht gefunden werden', 'error');
                        break;
                }
            });
        };

        vm.createRsvp = (form) => {
            if (!vm._validateRegistrationForm(form)) {
              return;
            }
            vm.submitting = true;
          
            if (!vm.rsvpInformation.hasOwnProperty("rsvp")) {
              vm.rsvpInformation.rsvp = {};
            }
          
            vm.rsvpInformation.rsvp.status = "confirmed";
          
            AirLSTSdkService.submitOpenRequest(vm._prepareRsvpInformationForApi()).then(
              (rsvpInfo) => {
                vm._init();
                vm.submitting = false;
                // form.$setPristine();
                // NOTE:
                form.$setPristine();
                vm.goToView('done');
            }, function(error) {
                vm.hasError = true;
                vm.submitting = false;

                switch (error.identifier) {
                    case 'validation':
                        SweetAlert.swal('Data incomplete', 'Please check your data.', 'error');
                        break;
                    case 'rsvp_not_found':
                    default:
                        SweetAlert.swal('Error', 'Technical problems. Please try again later.', 'error');
                        break;
                }
            });
          };

        /**
         * Cancels a rsvp
         *
         * vm.rsvpCode has to be set to a valid rsvp code for this call to succeed. Also the supplied form has to
         * pass validation.
         *
         * After successful cancellation, the form is reset, a success message is displayed and the 'code' view will
         * be shown next.
         *
         * If an error occurs during the API call an error message is displayed.
         *
         * @param form
         */
        vm.cancelRsvp = function(form) {
            if (form && !vm._validateRegistrationForm(form)) {
                return;
            }

            vm.submitting = true;
            AirLSTSdkService.updateRsvpByCodeAndStatus(vm.rsvpCode, 'cancelled', vm._prepareRsvpInformationForApi(), 'cancelled').then(function(rsvpInfo) {
                vm._init();
                vm.submitting = false;

                SweetAlert.swal('Schade', 'Deine Absage wurde uns übermittelt.', 'success');

                // resets the form
                // https://docs.angularjs.org/api/ng/type/form.FormController#$setPristine
                if (form) {
                    form.$setPristine();
                }

                vm.goToView('code');
            }, function(error) {
                vm.hasError = true;
                vm.submitting = false;

                switch (error.identifier) {
                    case 'validation':
                        SweetAlert.swal('Data incomplete', 'Bitte überprüfe deine Daten.', 'error');
                        break;
                    case 'rsvp_not_found':
                    default:
                        SweetAlert.swal('Error', 'Technical problems. Please try again later.', 'error');
                        break;
                }
            });
        };

        /**
         * Confirms a rsvp
         *
         * vm.rsvpCode has to be set to a valid rsvp code for this call to succeed. Also the supplied form has to
         * pass validation.
         *
         * After successful confirmation, the form is reset, a success message is displayed and the 'code' view will
         * be shown next.
         *
         * If an error occurs during the API call an error message is displayed.
         *
         * @param form
         */
        vm.confirmRsvp = function(form) {
            if (!vm._validateRegistrationForm(form)) {
                return;
            }
            vm.submitting = true;
            AirLSTSdkService.updateRsvpByCodeAndStatus(vm.rsvpCode, 'confirmed', vm._prepareRsvpInformationForApi(), 'confirmed').then(function(rsvpInfo) {
                vm._init();
                vm.submitting = false;

                // SweetAlert.swal('Vielen Dank', 'Ihre Rückmeldung wurde uns übermittelt. ', 'success');

                // resets the form
                // https://docs.angularjs.org/api/ng/type/form.FormController#$setPristine
                form.$setPristine();
                vm.goToView('done');
            }, function(error) {
                vm.hasError = true;
                vm.submitting = false;

                switch (error.identifier) {
                    case 'validation':
                        SweetAlert.swal('Data incomplete', 'Please check your data.', 'error');
                        break;
                    case 'rsvp_not_found':
                    default:
                        SweetAlert.swal('Error', 'Technical problems. Please try again later.', 'error');
                        break;
                }
            });
        };

        /**
         * Navigates to a different page
         *
         * @param viewToGoTo
         */
        vm.goToView = function(viewToGoTo) {
            vm.currentView = viewToGoTo;
            $scope.$applyAsync();
        };

        /**
         * Navigates to a different page
         *
         * @param viewToGoTo
         */
        vm.goToTab = function(tabToGoTo) {
            vm.currentTab = tabToGoTo;
            $scope.$applyAsync();
        };

        /**
         * Sets answer type
         * Is used as a helper for the landingpage to display different views
         *
         * Possible Values: undifined, cancel, confirmed
         * @param newAnswerType
         */
        vm.setAnswerType = function(newAnswerType) {
            vm.answerType = newAnswerType;
        };

        /**
         * Adds an empty contact and rsvp object in the guests array
         *
         * This is the place for pre-filling fields in the rsvp, contact, or guest objects *before* the user edits
         * them
         */
        vm.addGuest = function() {
            vm.rsvpInformation.guests.push({});
        };

        /**
         * Deletes the given guest object from the guest array
         *
         * @param guest
         */
        vm.removeGuest = function(guest) {
            var index = vm.rsvpInformation.guests.indexOf(guest);

            if (index > -1) {
                vm.rsvpInformation.guests.splice(index, 1);
            }
        };

        // Private internal functions

        /**
         * Checks if the current URL contains the rsvp_code GET parameter and tries to load the rsvp by the
         * supplied parameter
         *
         * @private
         */
        vm._checkRsvpCodeOnUrl = function() {
            var match = $location.absUrl().match(/rsvp_code=([A-Za-z0-9]+)/);

            if (match && match.length >= 2 && match[1]) {
                vm.rsvpCode = match[1];
                vm.loadRsvp();
            } else {
                vm.loading = false;
            }
            
        };


        /** POinti doc */
        vm._checkRsvpActionOnUrl = function() {
            var match = $location.absUrl().match(/action=([A-Za-z0-9]+)/);

            if (match && match.length >= 2 && match[1]) {
                return match[1];
            } else {
                return false;
            }
        };

        /**
         *
         * Validates the supplied form
         *
         * @param form
         * @returns {boolean}
         * @private
         */
        vm._validateRegistrationForm = function(form) {
            if (!form.$valid) {
                formErrors(form);
                SweetAlert.swal('Missing Data', 'Please fill in all required fields.', 'warning');
                return false;
            } else {
                return true;
            }
        };

        /**
         * Populates vm.eventInformation with guestlist data from the server
         *
         * @private
         */
        vm._retrieveEventInformation = function() {
            vm.eventInformation = null;

            AirLSTSdkService.retrieveGuestlistInformation().then(function(eventInformation) {
                vm.eventInformation = eventInformation;
            }, function() {
                vm.hasError = true;

                SweetAlert.swal('Error', 'Die Informationen zum gewünschten Event konnten nicht geladen werden', 'error');
            });
        };

        /**
         * Returns rsvp data ready to be submitted
         *
         * This is the place for changes to rsvp data, guest data etc *after* it was edited by the user but *before*
         * submitting it to the server
         *
         * @returns {*|null}
         * @private
         */
        vm._prepareRsvpInformationForApi = function() {

            /**
             * Sets a specific Information to the guests of guest. E.g. a value from the main guest
             */
            if (vm.rsvpInformation.hasOwnProperty('guests')) {
                vm.rsvpInformation.guests = vm.rsvpInformation.guests.map(function(guest) {
                    return guest;
                });
            }

            vm.rsvpInformation.rsvp.privacy_policy = !!vm.rsvpInformation.rsvp.privacy_policy;

            return vm.rsvpInformation;
        };

        /**
         * Transforms rsvp data supplied by the API
         *
         * @param inData
         * @returns {*}
         * @private
         */
        vm._prepareRsvpInformationFromApi = function(inData) {
            inData.rsvp.custom_1 = inData.rsvp.custom_1 ? inData.rsvp.custom_1 : null;
            inData.contact.country = inData.contact.country ? inData.contact.country : null;
            inData.contact.sex = inData.contact.sex ? inData.contact.sex : null;

            return inData;
        };

        vm._resetRsvpInformation = function() {
            vm.rsvpInformation = null;
        };

        vm._resetRsvpCode = function() {
            vm.rsvpCode = '';
        };

        vm._resetAnswerType = function() {
            vm.answerType = 'undefined';
        };

        vm._initCountDown = function() {
            // var eventStart = (new Date(vm.eventInformation.date)).getTime();
            var eventStart = (new Date(2021, 9, 28, 18, 0, 0)).getTime();
            var now = (new Date()).getTime();
            vm.countdownTime = Math.round((eventStart - now) / 1000);
        };

        function formErrors(form) {
            var errors = [];
            for (var key in form.$error) {
                errors.push(key + "=" + form.$error);
            }
            if (errors.length > 0) {
                console.log("Form Has Errors");
                console.log(form.$error);
            }
        };


        vm._init();
        vm._checkRsvpCodeOnUrl();
    }]);